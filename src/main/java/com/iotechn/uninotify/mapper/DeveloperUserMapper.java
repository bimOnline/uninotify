package com.iotechn.uninotify.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.iotechn.uninotify.domain.DeveloperUserDO;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: rize
 * Date: 2019/12/26
 * Time: 15:39
 */
public interface DeveloperUserMapper extends BaseMapper<DeveloperUserDO> {
}
