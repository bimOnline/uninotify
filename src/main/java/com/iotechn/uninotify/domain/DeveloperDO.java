package com.iotechn.uninotify.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: rize
 * Date: 2019/12/26
 * Time: 15:27
 */
@Data
@TableName("uninotify_developer")
public class DeveloperDO extends SuperDO implements Serializable {

    private String name;

    @TableField("contact_phone")
    private String contactPhone;

    @TableField("app_id")
    private String appId;

    @TableField("app_secret")
    private String appSecret;

    @TableField("notify_url")
    private String notifyUrl;

}
